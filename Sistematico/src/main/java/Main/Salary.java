/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sistemas11
 */
public class Salary {
    int id;
    String first_name;
    String last_name;
    String cedula;
    String cell;
    String direccion;
    int sal;
    String fot;
    String card;

    public Salary() {
    }

    public Salary(int id, String first_name, String last_name, String cedula, String cell, String direccion, int sal, String fot, String card) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.cedula = cedula;
        this.cell = cell;
        this.direccion = direccion;
        this.sal = sal;
        this.fot = fot;
        this.card = card;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setSal(int sal) {
        this.sal = sal;
    }

    public void setFot(String fot) {
        this.fot = fot;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public int getId() {
        return id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getCedula() {
        return cedula;
    }

    public String getCell() {
        return cell;
    }

    public String getDireccion() {
        return direccion;
    }

    public int getSal() {
        return sal;
    }

    public String getFot() {
        return fot;
    }

    public String getCard() {
        return card;
    }
     public List<String> toList() {
        List<String> docente = new ArrayList<>();

        docente.add(String.valueOf(card));
        docente.add(first_name);
        docente.add(last_name);
        docente.add(cedula);
        docente.add(direccion);
        docente.add(cell);
        docente.add(String.valueOf(sal));
        docente.add(fot);

        return docente;

    }
    
    
    
    
}
