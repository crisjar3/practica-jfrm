/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import com.google.gson.Gson;
import componentes.jtable.Docente;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Sistemas11
 */
public class SalTable extends AbstractTableModel{
    private List<Salary> data;
   private List<String> columnNames;
   
    public SalTable() {
        super();
        columnNames = new ArrayList<>();
        data = new ArrayList<>();
    }

    public SalTable(List<String> ColumnNames, List<Salary> data) {
        this.columnNames = ColumnNames;
        this.data = data;
    }
     public List<Salary> loadFromJson() throws FileNotFoundException {
        Gson gson = new Gson();
        data.addAll(Arrays.asList(gson.fromJson(new FileReader("resources/salary.json"),Salary[].class)));
        //String[] names = {"Codigo", "Nombres", "Apellidos", "Cedula", "Direccion", "Telefono", "salario*hora","Foto"};
        //columnNames = Arrays.asList(names);
        return data;
    }

    @Override
    public int getRowCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getColumnCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
